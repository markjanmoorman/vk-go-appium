Feature: This feature file is used for the POC appium (SEL-1519)

  Background:
    Given I navigate through the onboarding process

  @POC-iOS @xxyz
  Scenario: As a test automater I want to validate if the tabbar is highlighted after I clicked on it
    When I navigate to the "Best Read" section
    Then the "Best Read" section is shown
    When I navigate to the "News" section
    Then the "News" section is shown
    When I navigate to the "Service" section
    Then the "Service" section is shown
    When I navigate to the "Start" section
    Then the "Start" section is shown