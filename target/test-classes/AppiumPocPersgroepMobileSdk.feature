Feature: This feature file is used for the POC appium

  @xyz
  Scenario Outline: As a test automater I want to compare screenshots of articles against the baseline
    Given I set up the SDK editor for product: "Volkskrant", on feed: "Home", on environment: "ACC" and service: "ArticleStyleService"
    When I navigate to article id: "7038aa7"
    And I make screenshots for article id: 7038aa7
    Then I validate the screenshots for "<articleID>" against the baselines with "<faillureTreshold>"

    Examples:
      | articleID | faillureTreshold |
      | 7038aa7   | 0.15             |