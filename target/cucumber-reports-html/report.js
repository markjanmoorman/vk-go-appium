$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("AppiumPocPersgroepMobileSdk.feature");
formatter.feature({
  "line": 1,
  "name": "This feature file is used for the POC appium",
  "description": "",
  "id": "this-feature-file-is-used-for-the-poc-appium",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "As a test automater I want to compare screenshots of articles against the baseline",
  "description": "",
  "id": "this-feature-file-is-used-for-the-poc-appium;as-a-test-automater-i-want-to-compare-screenshots-of-articles-against-the-baseline",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@xyz"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I set up the SDK editor for product: \"Volkskrant\", on feed: \"Home\", on environment: \"ACC\" and service: \"ArticleStyleService\"",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to article id: \"7038aa7\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I make screenshots for article id: 7038aa7",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I validate the screenshots for \"\u003carticleID\u003e\" against the baselines with \"\u003cfaillureTreshold\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "this-feature-file-is-used-for-the-poc-appium;as-a-test-automater-i-want-to-compare-screenshots-of-articles-against-the-baseline;",
  "rows": [
    {
      "cells": [
        "articleID",
        "faillureTreshold"
      ],
      "line": 11,
      "id": "this-feature-file-is-used-for-the-poc-appium;as-a-test-automater-i-want-to-compare-screenshots-of-articles-against-the-baseline;;1"
    },
    {
      "cells": [
        "7038aa7",
        "0.15"
      ],
      "line": 12,
      "id": "this-feature-file-is-used-for-the-poc-appium;as-a-test-automater-i-want-to-compare-screenshots-of-articles-against-the-baseline;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 127539816,
  "status": "passed"
});
formatter.before({
  "duration": 15053148,
  "status": "passed"
});
formatter.before({
  "duration": 6201626,
  "status": "passed"
});
formatter.before({
  "duration": 7228376,
  "status": "passed"
});
formatter.before({
  "duration": 3894820459,
  "status": "passed"
});
formatter.before({
  "duration": 13611747,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "As a test automater I want to compare screenshots of articles against the baseline",
  "description": "",
  "id": "this-feature-file-is-used-for-the-poc-appium;as-a-test-automater-i-want-to-compare-screenshots-of-articles-against-the-baseline;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@xyz"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I set up the SDK editor for product: \"Volkskrant\", on feed: \"Home\", on environment: \"ACC\" and service: \"ArticleStyleService\"",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to article id: \"7038aa7\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I make screenshots for article id: 7038aa7",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I validate the screenshots for \"7038aa7\" against the baselines with \"0.15\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Volkskrant",
      "offset": 38
    },
    {
      "val": "Home",
      "offset": 61
    },
    {
      "val": "ACC",
      "offset": 85
    },
    {
      "val": "ArticleStyleService",
      "offset": 104
    }
  ],
  "location": "ArticleStepDefs.iSetUpTheSDKEditorForProductOnFeedOnEnvironmentTest(String,String,String,String)"
});
formatter.result({
  "duration": 3622315284,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "7038aa7",
      "offset": 27
    }
  ],
  "location": "ArticleStepDefs.iNavigateToArticleId(String)"
});
formatter.result({
  "duration": 3536046138,
  "status": "passed"
});
formatter.match({
  "location": "ArticleStepDefs.iMakeScreenshotsForArticle()"
});
formatter.result({
  "duration": 7021700817,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "7038aa7",
      "offset": 32
    },
    {
      "val": "0.15",
      "offset": 69
    }
  ],
  "location": "ImageComapareStepDefs.iValidateTheScreenshotsForAgaintTheBaselinesWith(String,Double)"
});
formatter.result({
  "duration": 1366590924,
  "status": "passed"
});
formatter.after({
  "duration": 55608,
  "status": "passed"
});
});