package stepdefs.editiongo;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.test.pageobject.editiongo.Tabbar;

import java.io.IOException;

/**
 * Created by Mark Jan Moorman on 2018-12-05.
 */
public class NavigationStepDefs {

    private Tabbar tabbar;

    @Before
    public void setUp() {
        tabbar = new Tabbar();
    }

    @When("^I navigate to the \"([^\"]*)\" section$")
    public void iNavigateToTheSection(String tabItem) {
        tabbar.clickTabItem(tabItem);
    }

    @Then("^the \"([^\"]*)\" section is shown$")
    public void theSectionIsShown(String sectionPage) {
        tabbar.verifySectionPage(sectionPage);
    }
}
