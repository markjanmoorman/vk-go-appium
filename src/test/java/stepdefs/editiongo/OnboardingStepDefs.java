package stepdefs.editiongo;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import nl.test.pageobject.device.OperatingSystemBased;
import nl.test.pageobject.editiongo.OnboardingPage;


/**
 * Created by Mark Jan Moorman on 28/10/2018.
 */

public class OnboardingStepDefs {

    private OnboardingPage onboardingPage;
    private OperatingSystemBased operatingSystemBased;

    @Before
    public void setUp() {
        onboardingPage = new OnboardingPage();
        operatingSystemBased = new OperatingSystemBased();
    }

    @And("^I navigate through the onboarding process$")
    public void iNavigateThroughTheOnboardingProcess() throws Throwable {
        if (onboardingPage.onboardingPresent()) {
            onboardingPage.getOnboardingContinueBtn().click();
            onboardingPage.getAgreeOnboardingBtn().click();
            onboardingPage.getFinishOnboardingBtn().click();
            operatingSystemBased.denyNotifications();
            operatingSystemBased.continueWithoutNotifications();
        }
    }

    @Given("^I validate screenshots via Applitools for \"([^\"]*)\" \"([^\"]*)\"$")
    public void iValidateScreenshotsViaApplitoolsForWithMatchLevel(String screen, String testname) throws Throwable {
        onboardingPage.validateScreenshotViaApplitools(screen, testname);
    }
}
