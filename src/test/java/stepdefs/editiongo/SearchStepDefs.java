package stepdefs.editiongo;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import nl.test.pageobject.editiongo.MorePage;
import nl.test.pageobject.device.OperatingSystemBased;
import nl.test.pageobject.editiongo.SearchPage;


/**
 * Created by Mark Jan Moorman on 2018-12-05.
 */
public class SearchStepDefs {

    private SearchPage searchPage;
    private MorePage morePage;
    private OperatingSystemBased operatingSystemBased;

    @Before
    public void setUp() {
        searchPage = new SearchPage();
        morePage = new MorePage();
        operatingSystemBased = new OperatingSystemBased();
    }

    @And("^I search for \"([^\"]*)\"$")
    public void iSearchFor(String searchInput) {
        searchPage.searchInput(searchInput);
        operatingSystemBased.getKeyboardSearch().click();

    }

    @And("^I click on search$")
    public void iClickOnSearch() {
        morePage.getSearch().click();
    }

    @And("^I click \"([^\"]*)\"$")
    public void iClick(String articleName) {
        searchPage.clickArticleName(articleName);
    }

    @Then("^the artikel \"([^\"]*)\" is shown$")
    public void theArtikelIsShown(String articleTitle) {
        searchPage.verifyArticleTitle(articleTitle);
    }
}
