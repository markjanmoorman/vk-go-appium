package stepdefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import nl.test.cucumber.TestSetupAndTeardown;

/**
 * Created by Mark Jan Moorman on 30/10/2018.
 */
public class CucumberHooksStepDefs {

    private TestSetupAndTeardown testSetupAndTeardown = new TestSetupAndTeardown();

    private final String cucumberTags =
                    "@xyz, " +
                    "@xyzz";

    /**
     * Before hooks is used for setting up a clean test environment
     */
    @Before({cucumberTags})
    public void setUp(Scenario scenario) {
        testSetupAndTeardown.setUp(scenario);
    }

    /**
     * Afterhook is used to create a screenshot when test has faild
     *
     * @param scenario cucumberscenario
     */
    @After({cucumberTags})
    public void tearDown(Scenario scenario) throws Throwable {
        testSetupAndTeardown.tearDown(scenario);
    }
}
