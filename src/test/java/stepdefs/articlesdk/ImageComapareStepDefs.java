package stepdefs.articlesdk;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import nl.test.pageobject.articlesdk.CompareImage;

import java.io.IOException;

/**
 * Created by Mark Jan Moorman on 2018-12-19.
 */
public class ImageComapareStepDefs {

    private CompareImage compareImage;

    @Before
    public void setUp() {
        compareImage = new CompareImage();
    }

    @Then("^I validate the screenshots for \"([^\"]*)\" against the baselines with \"([^\"]*)\"$")
    public void iValidateTheScreenshotsForAgaintTheBaselinesWith(String articleID, Double failureThreshold) throws IOException {
        compareImage.validateScreenshots(articleID, failureThreshold);
    }
}
