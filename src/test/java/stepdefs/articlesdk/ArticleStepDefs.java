package stepdefs.articlesdk;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.test.pageobject.articlesdk.Article;
import nl.test.pageobject.articlesdk.ArticleHomePage;

import java.io.IOException;

/**
 * Created by Mark Jan Moorman on 2018-12-13.
 */
public class ArticleStepDefs {

    private ArticleHomePage articleHomePage;
    private Article article;

    @Before
    public void setUp() {
        articleHomePage = new ArticleHomePage();
        article = new Article();
    }

    @Given("^I set up the SDK editor for product: \"([^\"]*)\", on feed: \"([^\"]*)\", on environment: \"([^\"]*)\" and service: \"([^\"]*)\"$")
    public void iSetUpTheSDKEditorForProductOnFeedOnEnvironmentTest(String product, String feed, String environment, String service) {
        article.getArticleSdkMenu().click();
        articleHomePage.setUpSdkEnvironment(product, feed, environment, service);
    }

    @Then("^I make screenshots for article id: \"([^\"]*)\"$")
    public void iMakeScreenshotsForArticle(String articleID) throws IOException {
        switch (articleID) {
            case "7038aa7":
                article.takeScreenshotsofArticle7038aa7();
                break;
            case "articleID":
                //to do add more code
                break;
            default:
                break;
        }
    }

    @When("^I navigate to article id: \"([^\"]*)\"$")
    public void iNavigateToArticleId(String articleId) {
        articleHomePage.searchingForArticle(articleId);
        articleHomePage.dismissAllWarnings();

    }
}
