package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import nl.framework.driver.DriverRegistry;
import nl.framework.driver.Platform;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Mark Jan Moorman on 28/10/2018.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber-reports/iOSRunnerBS.json", "html:target/cucumber-reports-html", "junit:target/cucumber-xml/iOSRunnerBS.xml"},
        features = "src/test/resources",
        glue = "stepdefs",
        tags = {"@POC-VK-GO"}
        )

public class iOSRunnerBrowerStack {

    private static DesiredCapabilities caps = new DesiredCapabilities();

    @BeforeClass
    public static void setUp() throws Throwable {
        caps.setCapability("deviceName", "iPhone 8");
        caps.setCapability("os_version", "12.1");
        caps.setCapability("realMobile", true);
        caps.setCapability("browserstack.local", "false");
        caps.setCapability("browserstack.localIdentifier", "persgroep");
        caps.setCapability("app", "VolkskrantAcc");
        caps.setCapability("browserstack.debug", true);
        caps.setCapability("browserstack.network", true);
        caps.setCapability("browserstack.networkLogs", true);
        DriverRegistry.setPlatform(Platform.IOS_BROWERSTACK, caps);
    }

    @AfterClass
    public static void teardown() {
        DriverRegistry.destroyDriver();
        System.out.println("Driver is closed");

    }
}
