package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import nl.framework.driver.DriverRegistry;
import nl.framework.driver.Platform;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Mark Jan Moorman on 28/10/2018.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber-reports/AndroidRunnerBS.json", "html:target/cucumber-reports-html", "junit:target/cucumber-xml/AndroidRunnerBS.xml"},
        features = "src/test/resources",
        glue = "stepdefs",
        tags = {"@POC-VK-GO"}
        )

public class AndroidRunnerBrowerStack {

    private static DesiredCapabilities caps = new DesiredCapabilities();

    @BeforeClass
    public static void setUp() throws Throwable {
        caps.setCapability("device", "Google Pixel");
        //caps.setCapability("app", "VKappAndroid");
        caps.setCapability("app", "VolkskrantAccAndroid");
        caps.setCapability("browserstack.local", false);
        caps.setCapability("browserstack.debug", true);
        caps.setCapability("browserstack.network", true);
        caps.setCapability("browserstack.networkLogs", true);
        DriverRegistry.setPlatform(Platform.ANDROID_BROWERSTACK, caps);
    }

    @AfterClass
    public static void teardown() {
        DriverRegistry.destroyDriver();
        System.out.println("Driver is closed");

    }
}
