package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import nl.framework.driver.DriverRegistry;
import nl.framework.driver.Platform;
import nl.framework.exceptions.DriverInitializationException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Mark Jan Moorman on 28/10/2018.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber-reports/AndroidRunner.json", "html:target/cucumber-reports-html", "junit:target/cucumber-xml/AndroidRunner.xml"},
        features = "src/test/resources",
        glue = "stepdefs",
        tags = {"@POC-VK-GO-Android"}
)

public class AndroidRunnerGo {

    private static DesiredCapabilities caps = new DesiredCapabilities();

    @BeforeClass
    public static void setUp() throws DriverInitializationException {
        caps.setCapability("automationName", "UiAutomator2");
        caps.setCapability("deviceName", "emulator-5554");
        caps.setCapability("platformName", "Android");
        caps.setCapability("avd", "Nexus_5X_API_27_2");
        caps.setCapability("fullReset", true);
        caps.setCapability("showChromedriverLog", true);
        caps.setCapability("disableWindowAnimation", true);
        caps.setCapability("autoLaunch", true);
        caps.setCapability("autoGrantPermissions", true);
        caps.setCapability("appWaitActivity", "nl.persgroep.edition.activities.live.onboarding.LiveOnboardingActivity");
        caps.setCapability("app", "/Users/mjmoorman/Projects/Persgroep/vk-appium-go/src/test/java/builds/VK-Go.apk");
        DriverRegistry.setPlatform(Platform.ANDROID_CUSTOM_LOCAL, caps);

    }

    @AfterClass
    public static void teardown() {
        DriverRegistry.destroyDriver();
        System.out.println("Driver is closed");
    }
}
