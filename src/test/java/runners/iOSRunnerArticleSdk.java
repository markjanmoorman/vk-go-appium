package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import nl.framework.driver.DriverRegistry;
import nl.framework.driver.Platform;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by Mark Jan Moorman on 28/10/2018.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber-reports/iOSRunner.json", "html:target/cucumber-reports-html", "junit:target/cucumber-xml/iOSRunner.xml"},
        features = "src/test/resources",
        glue = "stepdefs",
        tags = {"@POC-Article-SDK"}
        )

public class iOSRunnerArticleSdk {

    private static DesiredCapabilities caps = new DesiredCapabilities();

    @BeforeClass
    public static void setUp() throws Throwable {
        caps.setCapability("deviceName", "iPhone 8");
        caps.setCapability("automationName", "XCUITest");
        caps.setCapability("platformName", "iOS");
        caps.setCapability("platformVersion", "12.1");
        caps.setCapability("showXcodeLog", true);
        caps.setCapability("noReset", true);
        caps.setCapability("fullReset", false);
        caps.setCapability("autoLaunch", true);
        caps.setCapability("simpleIsVisibleCheck", true);
        caps.setCapability("app", "/Users/mjmoorman/Projects/Persgroep/vk-appium-go/src/test/java/builds/ArticleSdk.ipa");
        DriverRegistry.setPlatform(Platform.IOS_CUSTOM_LOCAL, caps);
    }

    @AfterClass
    public static void teardown() {
        DriverRegistry.destroyDriver();
        System.out.println("Driver is closed");
    }
}