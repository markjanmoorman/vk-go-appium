Feature: Applitools POC

  @Applitools-POC
  Scenario: As a test automater I would like to verify the layout of the Onboarding flow
    Given I validate screenshots via Applitools for "Onboarding" "Onboarding"

  @Applitools-POC @xyz
  Scenario: As a test automater I verify the layout of the VK-GO-HOME-FEED
    When I navigate through the onboarding process
    Given I validate screenshots via Applitools for "VK-GO-HOME" "VK-GO-HOME-FEED-TEST"
