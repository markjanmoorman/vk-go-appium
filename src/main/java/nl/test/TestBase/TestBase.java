package nl.test.TestBase;

import com.applitools.eyes.selenium.Eyes;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import nl.framework.driver.Driver;
import nl.framework.driver.DriverRegistry;
import nl.framework.driver.Platform;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Mark Jan Moorman on 28/10/2018.
 */
public class TestBase {

    private Driver driver;
    private Logger logger;
    private Platform platform;
    private Eyes eyes;


    public TestBase() {
        driver = DriverRegistry.getDriver();
        platform = DriverRegistry.getPlatform();
        logger = LoggerFactory.getLogger(TestBase.class);
        eyes = new Eyes();
        eyes.setApiKey("GHj100olQv2dIBuXo1jq8LId104ULECtpREBrJ52wijfkE4110");
        initNativeElements(this);
    }

    protected Driver getDriver() {
        return driver;
    }

    protected Logger getLogger() {
        return logger;
    }

    protected Eyes getEyes() { return eyes; }

    protected Platform platformValidator() {
        return platform;
    }

    // This will initialize the Mobile elements of the object based on the field decorator(s)
    private void initNativeElements(java.lang.Object obj) {
        PageFactory.initElements(new AppiumFieldDecorator(driver.getAppiumDriver()), obj);
    }

    // This will initialize the Mobile elements of the object based on the field decorator(s)
    private void initWebElements(java.lang.Object obj) {
        PageFactory.initElements(new AppiumFieldDecorator(driver.getRemoteWebDriver()), obj);
    }

    protected Boolean isAndroid() {
        return platformValidator().getDriverType().toString().equalsIgnoreCase("Android");
    }

    protected Boolean isiOS() {
        return platformValidator().getDriverType().toString().equalsIgnoreCase("iOS");
    }
}
