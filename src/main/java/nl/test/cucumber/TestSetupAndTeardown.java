package nl.test.cucumber;


import cucumber.api.Scenario;
import nl.framework.util.ApplicationActions;
import nl.framework.util.CaptureScreenShot;
import nl.test.TestBase.TestBase;

/**
 * Created by Mark Jan Moorman on 30/10/2018.
 */
public class TestSetupAndTeardown extends TestBase {


    public void setUp(Scenario scenario) {
        getLogger().info("Closing and opening the app for a clean start");
        getLogger().info("Starting test scenario: " + scenario.getName());
        ApplicationActions.closeApp(getDriver());
        ApplicationActions.openApp(getDriver());
    }

    public void tearDown(Scenario scenario) throws Throwable {
        if (scenario.isFailed()) {
            CaptureScreenShot.captureScreenShot(getDriver(), scenario);
        }
    }
}



