package nl.test.pageobject.articlesdk;

import nl.framework.util.ImageComparer;
import nl.test.TestBase.TestBase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * Created by Mark Jan Moorman on 2018-12-19.
 */
public class CompareImage extends TestBase {

    //todo Make device specific (extending on the below code, which is platform sepcific).

    public void validateScreenshots(String articleID, Double failureThreshold) throws IOException {
        getLogger().info("Comparing image(s) against baseline(s)");
        File folder = new File(getScreenshotOutputDirectory(articleID));
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                imageCompare(articleID, failureThreshold, listOfFiles[i].getName());
            }
        }
    }

    private void imageCompare(String articleID, Double failureThreshold, String fileName) throws IOException {
        final String baselineImagePath = getScreenshotBaselineDirectory(articleID) + "/" + fileName;
        Assert.assertTrue("Baseline image not present at location" + getScreenshotBaselineDirectory(articleID),
                Files.exists(Paths.get(baselineImagePath)));

        final File baselineImageFile = new File(baselineImagePath);
        final BufferedImage baseLineImage = ImageIO.read(baselineImageFile);
        final BufferedImage checkImage = ImageIO.read(new File(getScreenshotOutputDirectory(articleID) + "/" + fileName));
        final Pair<Double, BufferedImage> differencePair = ImageComparer.getDifference(baseLineImage, checkImage);

        getLogger().info("Difference for " + fileName + " compared to baseline is " + differencePair.getLeft().toString() + "%");
        getLogger().info("Publishing image compare result which can be found at: " + getDifferenceImageOutputDirectory(articleID) + "/" + getDifferenceImageFileName(articleID));

        final File differenceImageFile = new File(getDifferenceImageOutputDirectory(articleID) + "/" + getDifferenceImageFileName(fileName));
        FileUtils.forceMkdirParent(differenceImageFile);
        ImageIO.write(differencePair.getValue(), "png", differenceImageFile);

        Assert.assertTrue(
                "The differnce between the images is too high (threshold: " + failureThreshold + " actual difference: " + differencePair.getLeft().toString(),
                differencePair.getLeft() <= failureThreshold);
    }

    private String getScreenshotBaselineDirectory(String articleID) {
        if (isAndroid()) {
            return "target/screenshots/baselines/" + articleID + "-Android";
        } else {
            return "target/screenshots/baselines/" + articleID + "-iOS";
        }
    }


    private String getScreenshotOutputDirectory(String articleID) {
        if (isAndroid()) {
            return "target/screenshots/" + articleID + "-Android";
        } else {
            return "target/screenshots/" + articleID + "-iOS";
        }
    }

    private String getDifferenceImageOutputDirectory(String articleID) {
        if (isAndroid()) {
            return "target/screenshots/app-diff/" + articleID + "-Android";
        } else {
            return "target/screenshots/app-diff/" + articleID + "-iOS";
        }
    }

    private String getDifferenceImageFileName(String fileName) {
        if (isAndroid()) {
            String deviceName = getDriver().getCapabilities().getCapability("avd").toString();
            return fileName + "_article_comparison_" + deviceName + ".png";
        } else {
            String deviceName = getDriver().getCapabilities().getCapability("deviceName").toString();
            String platformVersion = getDriver().getCapabilities().getCapability("platformVersion").toString();
            return fileName + "_article_comparison_" + deviceName + "iOS" + platformVersion + ".png";
        }
    }
}


