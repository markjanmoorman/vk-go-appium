package nl.test.pageobject.articlesdk;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import nl.framework.util.CaptureScreenShot;
import nl.framework.util.Scroll;
import nl.framework.util.Sleeper;
import nl.test.TestBase.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

/**
 * Created by Mark Jan Moorman on 2018-12-14.
 */
public class Article extends TestBase {

    @iOSFindBy(accessibility = "LABEL HIER")
    @AndroidFindBy(xpath = "//*[@text='LABEL HIER']")
    private MobileElement article7038aa7;

    @iOSFindBy(accessibility = "Settings")
    @AndroidFindBy(accessibility = "Navigate up")
    private MobileElement articleSdkMenu;

    @AndroidFindBy(xpath = "//*[@text='Sub subheader hier']")
    private MobileElement article7038aa7ElementTwo;

    public MobileElement getArticleSdkMenu() {
        getDriver().waitForMobileElement(articleSdkMenu);
        return articleSdkMenu;
    }

    private void makeScreenshot(String article, String index, boolean android) throws IOException {
        CaptureScreenShot.captureScreenShotArticle(getDriver(), article, index, android);
    }

    private void screenshotsArticle7038aa7Android() throws IOException {
        makeScreenshot("7038aa7-Android", "1", true);
        Scroll.swipeByElements(article7038aa7ElementTwo, article7038aa7, getDriver());
        makeScreenshot("7038aa7-Android", "2", true);
    }

    private void screenshotsArticle7038aa7iOS() throws IOException {
        makeScreenshot("7038aa7-iOS", "1", false);
        Scroll.scrollDowniOS(getDriver());
        makeScreenshot("7038aa7-iOS", "2", false);
    }

    public void takeScreenshotsofArticle7038aa7() throws IOException {
        getDriver().waitForMobileElement(article7038aa7);
        getLogger().info("Making screenshots of article with article ID: 7038aa7");
        if (isAndroid()) {
            screenshotsArticle7038aa7Android();
        } else {
            screenshotsArticle7038aa7iOS();
        }
    }
}