package nl.test.pageobject.articlesdk;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import nl.test.TestBase.TestBase;

import java.util.List;

/**
 * Created by Mark Jan Moorman on 2018-12-13.
 */
public class ArticleHomePage extends TestBase {

    @iOSFindBy(accessibility = "Artikel ID or Article web URL")
    @AndroidFindBy(xpath = "//*[@text='Artikel ID or Article web URL']")
    private MobileElement searchInput;

    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Ga']")
    @AndroidFindBy(xpath = "//*[@text='GA']")
    private MobileElement go;

    @iOSFindBy(accessibility = "Done")
    @AndroidFindBy(xpath = "//*[@text='GA']")
    private MobileElement done;

    @iOSFindBy(accessibility = "De Volkskrant")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement productVolkskrant;

    @iOSFindBy(accessibility = "Parool")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement productParool;

    @iOSFindBy(accessibility = "De Morgen")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement productDeMorgen;

    @iOSFindBy(accessibility = "Trouw")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement productTrouw;

    @iOSFindBy(accessibility = "AD")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement productAd;

    @iOSFindBy(accessibility = "Acceptance")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement envAcc;

    @iOSFindBy(accessibility = "Production")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement envProd;

    @iOSFindBy(accessibility = "Home")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement feedHome;

    @iOSFindBy(accessibility = "Popular")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement feedPopular;

    @iOSFindBy(accessibility = "Recent")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement feedRecent;

    @iOSFindBy(accessibility = "Local Style Service")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement serviceLocalStyle;

    @iOSFindBy(accessibility = "Article Style Service")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement serviceArticleStyle;

    @iOSFindBy(accessibility = "Publisher Service")
    @AndroidFindBy(xpath = "TEST")
    private MobileElement servicePublisher;

    @iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='Dismiss All'])[2]")
    @AndroidFindBy(xpath = "//*[@text='Dismiss All']")
    private MobileElement dismissAllWarnings;

    private void selectListItemAndroid(String listItem) {
        List<MobileElement> menuItems = getDriver().getAppiumDriver().findElementsById("nl.persgroep.articledetail.app:id/design_menu_item_action_area");
        getDriver().waitForMobileElement(menuItems.get(0));
        switch (listItem) {
            case "Acc":
                menuItems.get(0).click();
                break;
            case "Prod":
                menuItems.get(1).click();
                break;
            case "ArticleStyleService":
                menuItems.get(2).click();
                break;
            case "Volkskrant":
                menuItems.get(3).click();
                break;
            case "Parool":
                menuItems.get(4).click();
                break;
            case "Home":
                menuItems.get(5).click();
                break;
            case "Popular":
                menuItems.get(6).click();
                break;
            case "Recent":
                menuItems.get(7).click();
                break;
        }
    }

    private void selectListItemiOS(String listItem) {
        switch (listItem) {
            case "Acc":
                envAcc.click();
                break;
            case "Prod":
                envProd.click();
                break;
            case "ArticleStyleService":
                serviceArticleStyle.click();
                break;
            case "LocalStyleService":
                serviceLocalStyle.click();
                break;
            case "PublisherService":
                servicePublisher.click();
                break;
            case "Volkskrant":
                productVolkskrant.click();
                break;
            case "Parool":
                productParool.click();
                break;
            case "De Morgen":
                productDeMorgen.click();
                break;
            case "Trouw":
                productTrouw.click();
                break;
            case "AD":
                productAd.click();
                break;
            case "Home":
                feedHome.click();
                break;
            case "Popular":
                feedPopular.click();
                break;
            case "Recent":
                feedRecent.click();
                break;
        }
    }

    public void setUpSdkEnvironment(String product, String feed, String environment, String service) {
        getLogger().info("Setting up environment variables for the article SDK");
        if (isAndroid()) {
            selectListItemAndroid(product);
            selectListItemAndroid(feed);
            selectListItemAndroid(environment);
            selectListItemAndroid(service);
        } else {
            selectListItemiOS(product);
            selectListItemiOS(feed);
            selectListItemiOS(environment);
            selectListItemiOS(service);

        }
        done.click();
    }

    public void searchingForArticle(String articleId) {
        getLogger().info("Navigating to article with article ID: " + articleId);
        searchInput.setValue(articleId);
        go.click();
    }

    public void dismissAllWarnings(){
        getLogger().info("Dismissing potential warnings for a clean screenshot");
        try {
            dismissAllWarnings.click();
        } catch (Exception e){
            System.out.println("Caught exception" + e);
        }
    }
}
