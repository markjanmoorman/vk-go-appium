package nl.test.pageobject.editiongo;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.MatchLevel;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import nl.framework.util.ContextView;
import nl.framework.util.Elements;
import nl.test.TestBase.TestBase;

/**
 * Created by Mark Jan Moorman on 2018-12-05.
 */
public class OnboardingPage extends TestBase {

    @iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='doorgaan'])[2]")
    @AndroidFindBy(accessibility = "doorgaan")
    private MobileElement onboardingContinue;

    @iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='ik ga akkoord'])[2]")
    @AndroidFindBy(xpath = "//*[@content-desc='ik ga akkoord']")
    private MobileElement agreeOnboarding;

    @iOSFindBy(xpath = "(//XCUIElementTypeOther[@name='start'])[2]")
    @AndroidFindBy(accessibility = "start")
    private MobileElement finishOnboarding;

    public MobileElement getOnboardingContinueBtn() throws Throwable {
        getDriver().waitForMobileElement(onboardingContinue);
        getDriver().switchToContext(ContextView.NATIVE);
        return onboardingContinue;
    }

    public MobileElement getAgreeOnboardingBtn() {
        getDriver().waitForMobileElement(agreeOnboarding);
        return agreeOnboarding;
    }

    public MobileElement getFinishOnboardingBtn() {
        getDriver().waitForMobileElement(finishOnboarding);
        return finishOnboarding;
    }

    public boolean onboardingPresent() throws Throwable {
        if (Elements.mobileElementIsPresent(onboardingContinue, getDriver())) {
            return true;
        } else
            return false;
    }

    public void validateScreenshotViaApplitools(String screen, String testName) {
        getEyes().setForceFullPageScreenshot(true);
        getEyes().shouldStitchContent();
        BatchInfo batchInfo = new BatchInfo("POC-Applitools");
        getEyes().setBatch(batchInfo);
        getEyes().setMatchLevel(MatchLevel.LAYOUT2);
        getEyes().open(getDriver().getRemoteWebDriver(), screen, testName);
        getEyes().checkWindow("Main view");
        getEyes().close();
    }
}
