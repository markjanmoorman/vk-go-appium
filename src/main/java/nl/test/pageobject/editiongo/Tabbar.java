package nl.test.pageobject.editiongo;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import nl.test.TestBase.TestBase;
import org.junit.Assert;

/**
 * Created by Mark Jan Moorman on 2018-12-05.
 */
public class Tabbar extends TestBase {

    @iOSFindBy(accessibility = "Start")
    @AndroidFindBy(xpath = "//*[@text='Start']")
    private MobileElement start;

    @iOSFindBy(accessibility = "Best gelezen")
    @AndroidFindBy(xpath = "//*[@text='Best gelezen']")
    private MobileElement bestRead;

    @iOSFindBy(accessibility = "Nieuws")
    @AndroidFindBy(xpath = "//*[@text='Nieuws']")
    private MobileElement news;

    @iOSFindBy(accessibility = "Service")
    @AndroidFindBy(xpath = "//*[@text='Service']")
    private MobileElement service;

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Nieuws']")
    @AndroidFindBy(accessibility = "test")
    private MobileElement newsValidator;


    private MobileElement getStart() {
        getDriver().waitForMobileElement(start);
        return start;
    }

    private MobileElement getBestRead() {
        getDriver().waitForMobileElement(bestRead);
        return bestRead;
    }

    private MobileElement getNews() {
        getDriver().waitForMobileElement(news);
        return news;
    }

    private MobileElement getService() {
        getDriver().waitForMobileElement(service);
        return service;
    }


    public void clickTabItem(String tabItem) {
        switch (tabItem) {
            case "Start":
                getStart().click();
                break;
            case "Best Read":
                getBestRead().click();
                break;
            case "News":
                getNews().click();
                break;
            case "Service":
                getService().click();
                break;
        }
    }

    public void verifySectionPage(String tabItem) {
        if (isiOS()) {
            switch (tabItem) {
                case "Start":
                    Assert.assertEquals("1", getStart().getAttribute("value"));
                    break;
                case "Best Read":
                    Assert.assertEquals("1", getBestRead().getAttribute("value"));
                    break;
                case "News":
                    Assert.assertEquals("1", getNews().getAttribute("value"));
                    break;
                case "Service":
                    Assert.assertEquals("1", getService().getAttribute("value"));
                    break;
            }
        } else {
            getLogger().info("Checks for Android to be implemented");
                //todo: built in checks for Android
        }
    }
}
