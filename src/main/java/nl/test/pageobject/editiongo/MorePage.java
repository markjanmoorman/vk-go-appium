package nl.test.pageobject.editiongo;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import nl.test.TestBase.TestBase;

/**
 * Created by Mark Jan Moorman on 2018-12-05.
 */
public class MorePage extends TestBase {

    @iOSFindBy(accessibility = "Zoeken Zoeken op auteur, titel of onderwerp")
    @AndroidFindBy(accessibility = "test")
    private MobileElement search;

    @iOSFindBy(accessibility = "Wat vindt u van deze app? We horen graag uw mening")
    @AndroidFindBy(accessibility = "test")
    private MobileElement giveFeedback;

    @iOSFindBy(accessibility = "Inloggen")
    @AndroidFindBy(accessibility = "test")
    private MobileElement logIn;

    @iOSFindBy(accessibility = "Pushberichten Ontvang notificaties van belangrijke berichten")
    @AndroidFindBy(accessibility = "test")
    private MobileElement push;

    @iOSFindBy(accessibility = "Klantenservice Beheer uw persoonlijke gegevens")
    @AndroidFindBy(accessibility = "test")
    private MobileElement customerSupport;

    @iOSFindBy(accessibility = "Veelgestelde vragen Alles wat u kunt verwachten binnen deze app")
    @AndroidFindBy(accessibility = "test")
    private MobileElement faq;

    @iOSFindBy(accessibility = "Privacy")
    @AndroidFindBy(accessibility = "test")
    private MobileElement privacy;

    @iOSFindBy(accessibility = "Topics Exclusief voor abonnees, alle artikelen uit 13 kranten")
    @AndroidFindBy(accessibility = "test")
    private MobileElement topics;

    @iOSFindBy(accessibility = "Inclusief Exclusieve aanbiedingen voor Volkskrantlezers")
    @AndroidFindBy(accessibility = "test")
    private MobileElement includingVK;

    @iOSFindBy(accessibility = "Winkel")
    @AndroidFindBy(accessibility = "test")
    private MobileElement shop;

    @iOSFindBy(accessibility = "Banen Vacatures voor hoger opgeleiden")
    @AndroidFindBy(accessibility = "test")
    private MobileElement jobs;

    @iOSFindBy(accessibility = "Tip de redactie")
    @AndroidFindBy(accessibility = "test")
    private MobileElement tipRedaction;

    public MobileElement getSearch() {
        getDriver().waitForMobileElement(search);
        return search;
    }

    public MobileElement getGiveFeedback() {
        getDriver().waitForMobileElement(giveFeedback);
        return giveFeedback;
    }

    public MobileElement getLogIn() {
        getDriver().waitForMobileElement(logIn);
        return logIn;
    }

    public MobileElement getPush() {
        getDriver().waitForMobileElement(push);
        return push;
    }

    public MobileElement getCustomerSupport() {
        getDriver().waitForMobileElement(customerSupport);
        return customerSupport;
    }

    public MobileElement getFaq() {
        getDriver().waitForMobileElement(faq);
        return faq;
    }

    public MobileElement getPrivacy() {
        getDriver().waitForMobileElement(privacy);
        return privacy;
    }

    public MobileElement getTopics() {
        getDriver().waitForMobileElement(topics);
        return topics;
    }

    public MobileElement getIncludingVK() {
        getDriver().waitForMobileElement(includingVK);
        return includingVK;
    }

    public MobileElement getShop() {
        getDriver().waitForMobileElement(shop);
        return shop;
    }

    public MobileElement getJobs() {
        getDriver().waitForMobileElement(jobs);
        return jobs;
    }

    public MobileElement getTipRedaction() {
        getDriver().waitForMobileElement(tipRedaction);
        return tipRedaction;
    }
}
