package nl.test.pageobject.editiongo;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import nl.test.TestBase.TestBase;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mark Jan Moorman on 2018-12-05.
 */
public class SearchPage extends TestBase {

    @iOSFindBy(accessibility = "Zoek")
    @AndroidFindBy(accessibility = "test")
    private MobileElement searchInput;

    @iOSFindBy(accessibility = "searchLiveViewerList")
    @AndroidFindBy(accessibility = "test")
    private MobileElement searchList;


    public void searchInput(String string) {
        searchInput.setValue(string);
    }

    public void clickArticleName(String article) {
        getDriver().waitForWebElement(By.xpath("//XCUIElementTypeButton[@name='" + article + "']"));
        getDriver().getAppiumDriver().findElement(By.xpath("//XCUIElementTypeButton[@name='" + article + "']")).click();
    }

    public void verifyArticleTitle(String articleTitle) {
        getDriver().waitForWebElement(By.xpath("//XCUIElementTypeStaticText[@name='" + articleTitle + "']"));
        assertEquals(articleTitle, getDriver().getAppiumDriver().findElement(By.xpath("//XCUIElementTypeStaticText[@name='" + articleTitle + "']")).getAttribute("name"));
    }
}
