package nl.test.pageobject.device;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import nl.framework.util.Elements;
import nl.test.TestBase.TestBase;

import static io.appium.java_client.android.nativekey.AndroidKey.ENTER;

/**
 * Created by Mark Jan Moorman on 2018-12-05.
 */
public class OperatingSystemBased extends TestBase {

    @iOSFindBy(accessibility = "Don’t Allow")
    @AndroidFindBy(accessibility = "test")
    private MobileElement dontAllow;

    @iOSFindBy(accessibility = "Allow")
    @AndroidFindBy(accessibility = "test")
    private MobileElement allow;

    @iOSFindBy(accessibility = "No device token")
    @AndroidFindBy(accessibility = "test")
    private MobileElement noDeviceToken;

    @iOSFindBy(accessibility = "Verder zonder notificaties")
    @AndroidFindBy(accessibility = "test")
    private MobileElement continueWithoutNotifications;

    @iOSFindBy(accessibility = "Search")
    @AndroidFindBy(accessibility = "test")
    private MobileElement keyboardSearch;

    public MobileElement getKeyboardSearch() {
        return keyboardSearch;
    }

    public void denyNotifications() throws Throwable {
        if (isAndroid()){
            denyNotificationsAndroid();
        } else {
            denyNotificationsiOS();
        }
    }

    private void denyNotificationsiOS() throws Throwable {
        if (Elements.mobileElementIsPresent(allow, getDriver())) {
            dontAllow.click();
        } else {
            continueWithoutNotifications.click();
        }
    }

    private void denyNotificationsAndroid() {
        getLogger().info("No notifications to deny for Android");
    }

    public void continueWithoutNotifications() throws Throwable {
        if (Elements.mobileElementIsPresent(continueWithoutNotifications, getDriver())) {
            continueWithoutNotifications.click();
        }
    }

    public void clickEnter() {
        String platform = platformValidator().getDriverType().toString();
        if (platform.equalsIgnoreCase("ANDROID")) {
            ((AndroidDriver) getDriver().getAppiumDriver()).pressKey(new KeyEvent(ENTER));
        } else if (platform.equalsIgnoreCase("IOS")) {
            System.out.println("Press enter");
        }
    }
}

