package nl.framework.exceptions;

import org.apache.commons.lang3.StringUtils;

import java.util.Set;

import static java.lang.String.format;

/**
 * Created by Mark Jan Moorman on 28/10/2018.
 */
public class ContextNotFoundException extends Exception {

    public ContextNotFoundException() {
    }

    public ContextNotFoundException(String message) {
        super(message);
    }

    public ContextNotFoundException(String contextLookingFor, Set<String> availableContexts) {
        super(format("Context %s not found in available contexts (%s).", contextLookingFor, StringUtils.join(availableContexts, ", ")));
    }

    public ContextNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ContextNotFoundException(Throwable cause) {
        super(cause);
    }

    public ContextNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
