package nl.framework.exceptions;

/**
 * Created by Mark Jan Moorman on 23/10/2018.
 */
public class DriverInitializationException extends Exception {

    public DriverInitializationException() {
    }

    public DriverInitializationException(String message) {
        super(message);
    }

    public DriverInitializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DriverInitializationException(Throwable cause) {
        super(cause);
    }

    public DriverInitializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
