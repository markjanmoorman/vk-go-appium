package nl.framework.driver;

/**
 * Created by Mark Jan Moorman on 23/10/2018.
 */
public enum Platform {
    // Local
    ANDROID_CUSTOM_LOCAL(Destination.LOCAL, DriverType.ANDROID),
    IOS_CUSTOM_LOCAL(Destination.LOCAL, DriverType.IOS),


    //Browsestack
    ANDROID_BROWERSTACK(Destination.BROWSERSTACK, DriverType.ANDROID),
    IOS_BROWERSTACK(Destination.BROWSERSTACK, DriverType.IOS);


    private Destination destination;
    private DriverType driverType;


    Platform(Destination destination, DriverType driverType) {
        this.destination = destination;
        this.driverType = driverType;
    }

    public enum Destination {BROWSERSTACK, LOCAL}

    public enum DriverType {ANDROID, IOS}

    public Destination getDestination() {
        return this.destination;
    }

    public DriverType getDriverType() {
        return this.driverType;
    }

    public static  String osVersion() {
        return String.valueOf(DriverRegistry.getDriver().getCapabilities().getCapability("platformVersion"));
    }

}

