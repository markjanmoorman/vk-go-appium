package nl.framework.driver;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import nl.framework.exceptions.ContextNotFoundException;
import nl.framework.util.Sleeper;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static nl.framework.driver.Platform.DriverType.ANDROID;


/**
 * Created by Mark Jan Moorman on 23/10/2018.
 */
public class Driver {

    private Logger LOG;
    protected RemoteWebDriver driver;
    private int timeout = 30;
    private static Platform platform = DriverRegistry.getPlatform();


    /**
     * The driver that is used for both selenium RemoteWebdriver and Appium Drivers to control web and app contexts.
     * initialization is done by {@link DriverRegistry(Platform)}
     * or {@link DriverRegistry(Platform, DesiredCapabilities )}.
     *
     * @param remoteWebDriver The driver that can be used for automation.
     */
    public Driver(RemoteWebDriver remoteWebDriver) {
        LOG = LoggerFactory.getLogger(Driver.class);
        this.driver = remoteWebDriver;
    }

    /**
     * Get the driver casted to an AppiumDriver object
     *
     * @return AppriumDriver
     */
    public AppiumDriver getAppiumDriver() {
        return (AppiumDriver) driver;
    }

    public RemoteWebDriver getRemoteWebDriver() {
        return (RemoteWebDriver) driver;
    }


    /**
     * Method to obtain the set and obtained capabilities by the driver.
     *
     * @return {@link Capabilities}
     */
    public Capabilities getCapabilities() {
        return driver.getCapabilities();
    }

    /**
     * This method will check every 10 milliseconds that the element in question has reached a clickable state and then clicks it
     *
     * @param selector - the By type object that points to the element we are waiting to click on
     */
    public void click(By selector) {
        waitForWebElement(selector);
        getElementWhenVisible(selector).click();
    }

    /**
     * Returns the WebElement after waiting for it to be visible
     *
     * @param selector the By selection used to identify the WebElement
     * @return a WebElement
     */
    public WebElement getElementWhenVisible(By selector) {
        return getElementWithexpectedCondition(ExpectedConditions.visibilityOfElementLocated(selector));
    }

    private WebElement getElementWithexpectedCondition(ExpectedCondition<WebElement> condition) {
        WebDriverWait wait = new WebDriverWait(getAppiumDriver(), 10); //seconds
        return wait
                .ignoring(StaleElementReferenceException.class, NoSuchElementException.class)
                .pollingEvery(Duration.ofSeconds(10))
                .until(condition);
    }

    /**
     * Sets the timeout for implicit waits.
     *
     * @param timeout in Seconds.
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
        driver.manage().timeouts().implicitlyWait(this.timeout, TimeUnit.SECONDS);
    }

    /**
     * Forward of {@link RemoteWebDriver#quit()}.
     */
    public void quit() {
        driver.quit();
    }

    /**
     * Waits for the given webelement to be present
     *
     * @param selector - the By type object that points to the element we are waiting to click on
     */
    public void waitForWebElement(By selector) {
       swithToWindoHandle(selector);
        try {
            WebDriverWait wait = new WebDriverWait(getAppiumDriver(), 10);
            wait.ignoring(StaleElementReferenceException.class, NoSuchElementException.class)
                    .pollingEvery(Duration.ofMillis(100))
                    .until(ExpectedConditions.elementToBeClickable(selector));
        } catch (Exception e) {
            System.out.println("Something went wrong... " + e.getMessage());
        }
    }

    /**
     * Waits for MobileElement to be present
     *
     * @param selector the MobileElement selector to identify the element
     */
    public void waitForMobileElement(MobileElement selector) {
        try {
            WebDriverWait wait = new WebDriverWait(getAppiumDriver(), 10);
            wait.ignoring(StaleElementReferenceException.class, NoSuchElementException.class)
                    .pollingEvery(Duration.ofMillis(100))
                    .until(ExpectedConditions.elementToBeClickable(selector));
        } catch (Exception e) {
            System.out.println("Something went wrong... " + e.getMessage());
        }
    }

    /**
     * Switches to the windowhandle were element is located
     *
     * @param selector - the By type object that points to the element we are waiting to click on
     */
    private void swithToWindoHandle(By selector) {
        if (platform.getDriverType().equals(ANDROID)) {
            Boolean shouldCheck = true;
            for (String windowHandle : getAppiumDriver().getWindowHandles()) {
                if (shouldCheck) {
                    driver.switchTo().window(windowHandle);
                    List list = getAppiumDriver().findElements(selector);
                    if (!list.isEmpty()) {
                        shouldCheck = false;
                    }
                }
            }
        }
    }


    /**
     * Switches to requested context
     *
     * @param context - context that is requested
     */
    public void switchToContext(String context) throws Throwable {
        waitForContext(context);
        switchToContexts(context);
    }

    /**
     * Waits for requested context
     *
     * @param context - context that is requested
     */
    private void waitForContext(String context) {
        long start_time = System.currentTimeMillis();
        long wait_time = 15000;
        long end_time = start_time + wait_time;
        context = context.toUpperCase();

        while ((System.currentTimeMillis() < end_time) && (!getAppiumDriver().getContextHandles().toString().contains(context))) {
            Sleeper.sleep(1000);
            System.out.println(getAppiumDriver().getContextHandles());
        }
    }

    /**
     * Sets the context
     *
     * @param context The context parameter in String format e.g. "WEBVIEW" or "NATIVE_APP". The first context starting
     *                with this value will be selected. Timeout is hardcoded and set to 1 minute
     */
    private void switchToContexts(String context) throws ContextNotFoundException {
        outerloop:
        for (int i = 0; i < 120; i++) {
            Set<String> contexts = ((AppiumDriver) driver).getContextHandles();
            LOG.debug("available contexts: {}", contexts.toString());
            for (String record : contexts) {
                if (record.toUpperCase().startsWith(context.toUpperCase())) {
                    ((AppiumDriver) driver).context(record);
                    break outerloop;
                }
            }
            if (i == 119) {
                ContextNotFoundException e = new ContextNotFoundException(context, contexts);
                LOG.error(e.getMessage());
                throw e;
            }
            Sleeper.sleep(1500);
        }
        LOG.debug("Context after switching: {}", ((AppiumDriver) driver).getContext());
    }

    /**
     * Get the screenshot in the type of target parameter.
     *
     * @param target the output type e.g. OutputType.BYTES.
     * @param <X>
     * @return The screenshot content in format of parameter target.
     */
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        return this.driver.getScreenshotAs(target);
    }
}
