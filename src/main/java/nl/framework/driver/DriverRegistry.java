package nl.framework.driver;


import nl.framework.exceptions.DriverInitializationException;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.framework.util.Sleeper;

import java.net.URL;

/**
 * Created by Mark Jan Moorman on 23/10/2018.
 */
public class DriverRegistry {

    private static Driver driver;
    private static Platform platform;
    private static DesiredCapabilities customCapabilities = null;
    private static Logger LOG;


    /**
     * Sets {@link Platform} for driver initialization.
     *
     * @param platformName The platform to set, enums can be found in {@link Platform} class.
     * @return The driver instance
     */
    private static Driver setPlatform(Platform platformName) throws DriverInitializationException {
        platform = platformName;
        initialize();
        return driver;
    }

    public static Platform getPlatform() {
        return platform;
    }

    /**
     * Sets {@link Platform} for driver initialization for a custom device.
     *
     * @param platformName The platform to set, enums can be found in {@link Platform} class.
     * @return The RabobankDriver instance
     */
    public static Driver setPlatform(Platform platformName, DesiredCapabilities capabilities) throws DriverInitializationException {
        customCapabilities = capabilities;
        return setPlatform(platformName);
    }


    /**
     * The method that initializes a {@link Driver} and returns it.
     *
     * @return The {@link Driver} to be used (based on Selenium {@link RemoteWebDriver}, {@link AndroidDriver},
     * or {@link IOSDriver}.
     */
    public static Driver getDriver() {
        if (driver == null) {
            try {
                initialize();
            } catch (DriverInitializationException e) {
                return null;
            }
        }
        return driver;
    }

    /**
     * The method to quit the browser or application under test and to empty the driver object. Useful for ending every
     * test.
     */
    public static void destroyDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }


    private static void initialize() throws DriverInitializationException {
        LOG = LoggerFactory.getLogger(DriverRegistry.class);
        try {
            DesiredCapabilities capabilities = new DesiredCapabilities();

            URL automationURL;

            if (platform.getDestination() == Platform.Destination.BROWSERSTACK) {
                automationURL = new URL("http://teamamsterdam2:eQVfHnqSzhncirye6ZMJ@hub-cloud.browserstack.com/wd/hub");
            } else {
                automationURL = new URL("http://localhost:4723/wd/hub");
            }

            //finally, merge custom capabilities and create driver:
            switch (platform.getDriverType()) {
                case ANDROID:
                    AndroidDriver androidDriver = new AndroidDriver(automationURL, capabilities.merge(customCapabilities));
                    driver = new Driver(androidDriver);
                    Sleeper.sleep(2000);
                    break;
                case IOS:
                    IOSDriver iOSDriver = new IOSDriver(automationURL, capabilities.merge(customCapabilities));
                    driver = new Driver(iOSDriver);
                    Sleeper.sleep(2000);
                    break;
            }
        } catch (Exception e) {
            DriverInitializationException ex = new DriverInitializationException("Couldn't initialize driver, caused by: " + e.getMessage(), e);
            LOG.error(ex.getMessage());
            throw ex;
        }
    }
}
