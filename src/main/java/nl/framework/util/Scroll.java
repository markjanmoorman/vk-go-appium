package nl.framework.util;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import nl.framework.driver.Driver;
import nl.test.TestBase.TestBase;

import java.util.HashMap;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

/**
 * Created by Mark Jan Moorman on 2018-12-18.
 */
public class Scroll extends TestBase {

    /**
     * Scroll to child element
     * Use this method to scroll to an textView element by providing the text of the element
     * All elements need to be unique
     *
     * @param scrollableList = Scrollable container
     * @param text           = Text of the element you want to scroll to
     * @param driver         = driver instance
     */
    public static MobileElement scrollToElementUsingGetChildByText(String scrollableList, String text, Driver driver) {
        MobileElement element = (MobileElement) driver.getAppiumDriver().findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().resourceId(\"" + scrollableList + "\")).getChildByText("
                        + "new UiSelector().className(\"android.widget.TextView\"), \"" + text + "\")"));
        return element;
    }

    /**
     * Scroll until element is in view
     * Use this method to scroll to an textView element by providing the text of the element.
     * All elements need to be unique
     *
     * @param scrollableList = Scrollable container
     * @param text           = Text of the element you want to scroll to
     * @param driver         = driver instance
     */
    public static MobileElement scrollToElementUsingScrollIntoView(String scrollableList, String text, Driver driver) {
        MobileElement element = (MobileElement) driver.getAppiumDriver().findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector()"
                        + ".resourceId(\"" + scrollableList + "\")).scrollIntoView("
                        + "new UiSelector().text(\"" + text + "\"));"));
        return element;
    }

    /**
     * Scroll to contend-desc
     * Use this method to scroll to an textView element by providing the text of the element.
     * All elements need to be unique
     *
     * @param text           = Text of the element you want to scroll to
     * @param driver         = driver instance
     */
    public static MobileElement scrollToElementUsingGetContentDesc(String text, Driver driver) {
        MobileElement element = (MobileElement) driver.getAppiumDriver().findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().resourceId(\"nl.persgroep.articledetail.app:id/articledetailfragment\")).getChildByDescription("
                        + "new UiSelector().className(\"android.view.ViewGroup\"), \"" + text + "\")"));
        return element;
    }

    /**
     * Swipe by elements
     * Use this method to swipe from one particular element to another. This is the safest scroll method since this will always scroll the same since it is based on coordinates.
     * All elements need to be unique
     *
     * @param startElement   = MobileElement coordinates to start the swipe from
     * @param endElement     = MobileElement coordinates to end the swipe with
     * @param driver         = driver instance                      
     */
    public static void swipeByElements(MobileElement startElement, MobileElement endElement, Driver driver) {
        int startX = startElement.getLocation().getX() + (startElement.getSize().getWidth() / 2);
        int startY = startElement.getLocation().getY() + (startElement.getSize().getHeight() / 2);

        int endX = endElement.getLocation().getX() + (endElement.getSize().getWidth() / 2);
        int endY = endElement.getLocation().getY() + (endElement.getSize().getHeight() / 2);

        new TouchAction(driver.getAppiumDriver())
                .press(point(startX, startY))
                .waitAction(waitOptions(ofMillis(500)))
                .moveTo(point(endX, endY))
                .release().perform();
        Sleeper.sleep(100);
    }

    /**
     * Use this method to scroll a view unit down for iOS
     *
     * @param driver = driver instance
     */
    public static void scrollDowniOS(Driver driver) {
        HashMap<String, String> scrollObject = new HashMap<>();
        scrollObject.put("direction", "down");
        driver.getAppiumDriver().executeScript("mobile:scroll", scrollObject);
        Sleeper.sleep(750);
    }
}