package nl.framework.util;

import cucumber.api.Scenario;
import nl.framework.driver.Driver;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mark Jan Moorman on 31/10/2018.
 */
public class CaptureScreenShot {

    private static String timestamp() {
        return new SimpleDateFormat(" dd-MM-yyyy HH:mm:ss").format(new Date());
    }

    public static void captureScreenShot(Driver driver, Scenario scenario) throws IOException {
        byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
        File screenshotdir = new File("target/screenshots");
        screenshotdir.mkdirs();
        FileUtils.writeByteArrayToFile(new File(screenshotdir.getAbsolutePath() + "/" + scenario.getName() + timestamp() + ".png"), screenshot);
    }

    public static void captureScreenShotArticle(Driver driver, String article, String index, boolean android) throws IOException {
        byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);

        File screenshotdir = new File("target/screenshots/" + article);
        File fileLocation = new File(screenshotdir.getAbsolutePath() + "/" + article + "-" + index + ".png");
        if (android) {
            FileUtils.writeByteArrayToFile(fileLocation, removeStatusBarAndroid(screenshot));
        } else {
            FileUtils.writeByteArrayToFile(fileLocation, removeStatusBariOS(screenshot));
        }
    }

    private static byte[] removeStatusBarAndroid(final byte[] screenshot) throws IOException {
        InputStream in = new ByteArrayInputStream(screenshot);
        BufferedImage bufferedImage = ImageIO.read(in);

        int color;
        int gray = bufferedImage.getRGB(1, 0);
        int y = 1;

        do {
            y++;
            color = bufferedImage.getRGB(1, y);
        } while (color == gray);

        final BufferedImage imageWithNoStatusBar = bufferedImage.getSubimage(0, y, bufferedImage.getWidth(), bufferedImage.getHeight() - y);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(imageWithNoStatusBar, "png", byteArrayOutputStream);
        byteArrayOutputStream.flush();
        final byte[] imageInByte = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return imageInByte;
    }

    private static byte[] removeStatusBariOS(final byte[] screenshot) throws IOException {

        InputStream in = new ByteArrayInputStream(screenshot);
        BufferedImage bufferedImage = ImageIO.read(in);

        final int blue = bufferedImage.getRGB(38, 67);
        //colorcode blue arrow: 16024833

        for (int y = 0; y < 100; y++) {
            for (int x = 0; x < 50; x++) {
                int color = bufferedImage.getRGB(x, y);
                int cutOffCorrectionY = y - 15;
                if (color == blue) {
                    final BufferedImage imageWithNoStatusBar = bufferedImage.getSubimage(0, cutOffCorrectionY, bufferedImage.getWidth(), bufferedImage.getHeight() - cutOffCorrectionY);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    ImageIO.write(imageWithNoStatusBar, "png", byteArrayOutputStream);
                    byteArrayOutputStream.flush();
                    final byte[] imageInByte = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    return imageInByte;
                }
            }
        }
        // If blue is not present then return screenshot
        return screenshot;
    }
}