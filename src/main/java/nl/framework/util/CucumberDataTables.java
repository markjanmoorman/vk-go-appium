package nl.framework.util;

import cucumber.api.DataTable;

import java.util.List;

/**
 * Created by Mark Jan Moorman on 09/11/2018.
 */
public class CucumberDataTables  {

    //use for single colums
    public void cucumberDataTable(DataTable dialogBox) {
        //outer list is rows and inner list is columns
        List<List<String>> table = dialogBox.raw();
        for (List<String> rows : table) {
            for (String column : rows) {
                //doStuff(column);
            }
        }
    }


    //use for multiple columns
    public void cucumberDataTablev2(DataTable dialogBox) {
        //outer list is rows and inner list is columns
        List<List<String>> table = dialogBox.raw();
        for (List<String> rows : table) {
            for (int i = 0; i < rows.size(); i++) {
                if (i == 0) {
                    String column0 = rows.get(i);
                    //Dosometing(column0);
                }
            }
        }
    }
}
