package nl.framework.util;

import nl.framework.driver.Driver;
import nl.test.TestBase.TestBase;

import java.time.Duration;

/**
 * Created by Mark Jan Moorman on 30/10/2018.
 */
public class ApplicationActions {

    public static void openApp(Driver driver) {
        driver.getAppiumDriver().launchApp();
    }

    public static void closeApp(Driver driver) {
        driver.getAppiumDriver().closeApp();
    }

    public static void backgroundApp(Driver driver, Duration duration) {
        driver.getAppiumDriver().runAppInBackground(duration);
    }

}
