package nl.framework.util;

import io.appium.java_client.MobileElement;
import nl.framework.driver.Driver;
import nl.test.TestBase.TestBase;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

/**
 * Created by Mark Jan Moorman on 29/10/2018.
 */
public class Elements extends TestBase {


    /**
     * Use this method to validate if a mobile element is present or not
     *
     * @param locator = locator for the MobileElement
     * @param driver  = driver instance
     * @throws Throwable
     */
    public static boolean mobileElementIsPresent(MobileElement locator, Driver driver) throws Throwable {
        try {
            locator.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Use this method to validate if a mobile element is not located
     *
     * @param locator = locator for the WebElement
     * @param driver  = driver instance
     * @throws Throwable
     */
    public static boolean webElementIsNotLocated(WebElement locator, Driver driver, String context) throws Throwable {
        driver.switchToContext(context);
        Boolean isDisplayed;
        try {
            locator.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}

