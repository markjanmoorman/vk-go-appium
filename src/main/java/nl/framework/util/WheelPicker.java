package nl.framework.util;

import io.appium.java_client.MobileElement;
import nl.framework.driver.Driver;
import nl.framework.driver.DriverRegistry;
import nl.framework.driver.Platform;
import org.openqa.selenium.WebDriverException;

import static nl.framework.driver.Platform.DriverType.ANDROID;

/**
 * Created by Mark Jan Moorman on 06/11/2018.
 */
public class WheelPicker {

    private static Platform platform = DriverRegistry.getPlatform();

    //Datepicker method for iOS
    private static void scrollKeysIOS(Driver driver, String[] list) throws WebDriverException {
        for (int i = 0; i < list.length; i++) {
            if (Platform.osVersion().equals("11")) {
                MobileElement we = (MobileElement) driver.getAppiumDriver().findElementByXPath("//XCUIElementTypePickerWheel[" + (i + 1) + "]");
                we.sendKeys(list[i]);
            } else {
                MobileElement we = (MobileElement) driver.getAppiumDriver().findElementByXPath("//UIAPickerWheel[" + (i + 1) + "]");
                we.sendKeys(list[i]);
            }
        }
    }

    public void scrollKeys(Driver driver, String[] list) {
        if (platform.getDriverType().equals(ANDROID)) {
            try {
                scrollKeysIOS(driver, list);
            } catch (WebDriverException e) {
                System.out.println(e);
            }
        }
    }
}
