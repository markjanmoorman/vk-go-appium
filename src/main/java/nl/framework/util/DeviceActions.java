package nl.framework.util;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.ios.IOSDriver;
import nl.framework.driver.Driver;
import nl.framework.driver.DriverRegistry;
import nl.framework.driver.Platform;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.UnsupportedCommandException;

import java.time.Duration;

import static io.appium.java_client.android.nativekey.AndroidKey.*;
import static nl.framework.driver.Platform.DriverType.ANDROID;

/**
 * Created by Mark Jan Moorman on 29/10/2018.
 */
public class DeviceActions {

    private static Platform platform = DriverRegistry.getPlatform();


    public static void rotateDeviceToLandscape(Driver driver) {
        driver.getAppiumDriver().rotate(ScreenOrientation.LANDSCAPE);
    }

    public static void rotateDeviceToPortrait(Driver driver) {
        driver.getAppiumDriver().rotate(ScreenOrientation.PORTRAIT);
    }


    /**
     * ANDROID ONLY
     */
    private static void lockAndUnlockAndroid(Driver driver, Duration duration) {
        ((AndroidDriver) driver.getAppiumDriver()).lockDevice(duration);
        ((AndroidDriver) driver.getAppiumDriver()).unlockDevice();
    }

    public static void clickHardwareBackAndroid(Driver driver) throws UnsupportedCommandException {
        ((AndroidDriver) driver.getAppiumDriver()).pressKey(new KeyEvent(BACK));
    }

    public static void clickHardwareHomeAndroid(Driver driver) throws UnsupportedCommandException {
        ((AndroidDriver) driver.getAppiumDriver()).pressKey(new KeyEvent(HOME));
    }

    public static void clickHardwareMenuAndroid(Driver driver) throws UnsupportedCommandException {
        ((AndroidDriver) driver.getAppiumDriver()).pressKey(new KeyEvent(MENU));
    }

    public static void clickEnterAndroid(Driver driver) throws UnsupportedCommandException {
        ((AndroidDriver) driver.getAppiumDriver()).pressKey(new KeyEvent(ENTER));
    }


    /**
     * iOS ONLY
     */
    private static void lockAndUnlockiOS(Driver driver, Duration duration) {
        ((IOSDriver) driver.getAppiumDriver()).lockDevice(duration);
    }

    public static void shakeIOS(Driver driver) {
        ((IOSDriver) driver.getAppiumDriver()).shake();
    }


    /**
     * Universal (combining iOS and Android methods)
     */
    public static void lockAndUnlockDevice(Driver driver, Duration duration) {
        if (platform.getDriverType().equals(ANDROID)) {
            lockAndUnlockAndroid(driver, duration);
        } else {
            lockAndUnlockiOS(driver, duration);
        }
    }
}

