package nl.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Mark Jan Moorman on 23/10/2018.
 */
public class Sleeper {

    public static void sleep(int milli) {
        Logger LOG = LoggerFactory.getLogger(Sleeper.class);
        try {
            Thread.sleep(milli);
        } catch (InterruptedException e) {
            LOG.error("Something went wrong during sleeping", e);
        }
    }
}