package nl.framework.util;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.awt.image.BufferedImage;


/**
 * Based on algorithm found at: https://www.geeksforgeeks.org/image-processing-java-set-14-comparison-two-images/
 */

public class ImageComparer {

    private static final int SEPARATOR_SIZE = 2;

    public static Pair<Double, BufferedImage> getDifference(final BufferedImage baselineImage, final BufferedImage checkImage) {
        final int baselineImageWidth = baselineImage.getWidth();
        final int baselineImageHeight = baselineImage.getHeight();
        final int checkImageWidth = checkImage.getWidth();
        final int checkImageHeight = checkImage.getHeight();
        final int maxWidth = checkImageWidth > baselineImageWidth ? checkImageWidth : baselineImageWidth;
        final int maxHeight = checkImageHeight > baselineImageHeight ? checkImageHeight : baselineImageHeight;
        final BufferedImage differenceImage = new BufferedImage((maxWidth + SEPARATOR_SIZE) * 3, maxHeight, BufferedImage.TYPE_INT_RGB);

        long totalColorValueDifferences = 0;
        for (int y = 0; y < maxHeight; y++) {
            boolean isBaseLineImageWithinHeight = baselineImageHeight > y;
            boolean isCheckImageWithinHeight = checkImageHeight > y;
            for (int x = 0; x < maxWidth; x++) {
                boolean isBaseLineImageWithinWidth = baselineImageWidth > x;
                boolean isCheckImageWithinWidth = checkImageWidth > x;
                int rgbBaseLine = (isBaseLineImageWithinHeight && isBaseLineImageWithinWidth) ? baselineImage.getRGB(x, y) : 0;
                int rgbCheck = (isCheckImageWithinHeight && isCheckImageWithinWidth) ? checkImage.getRGB(x, y) : 0;
                final Triple<Integer, Integer, Integer> absoluteRGBDifferences = getAbsoluteRGBDifferences(rgbBaseLine, rgbCheck);
                final int pixelDifference = absoluteRGBDifferences.getLeft() + absoluteRGBDifferences.getMiddle() + absoluteRGBDifferences.getRight();
                totalColorValueDifferences = totalColorValueDifferences + pixelDifference;
                final int pixelDifferenceRGB = convertDifferenceToRGB(pixelDifference);
                differenceImage.setRGB(x, y, rgbBaseLine); // Set baseline part
                differenceImage.setRGB(x + SEPARATOR_SIZE + maxWidth, y, rgbCheck); // Set check part
                differenceImage.setRGB(x + ((SEPARATOR_SIZE + maxWidth) * 2), y, pixelDifferenceRGB); // Set difference/heatmap part
            }
        }

        // There are maxWidth * maxHeight pixels.
        // Normalizing the color channels (i.e. we regard each color channel per pixel as an individual pixel)
        // means there are 3 * maxWidth * maxHeight pixels
        final int totalPixels = 3 * maxWidth * maxHeight;

        // Normalizing the total accumulated value of color value differences gets an average difference for the image.
        final double averageDifference = (double) totalColorValueDifferences / (double) totalPixels;

        // Converting to a percentage based on the fact that there are 255 discrete values per pixel.
        final double percentageDifferences = (averageDifference / 255) * 100;

        return Pair.of(percentageDifferences, differenceImage);
    }

    private static Triple<Integer, Integer, Integer> getAbsoluteRGBDifferences(final int rgbLeft, final int rgbRight) {
        final Triple<Integer, Integer, Integer> rgbValuesLeft = getRedGreenBlueValues(rgbLeft);
        final Triple<Integer, Integer, Integer> rgbValuesRight = getRedGreenBlueValues(rgbRight);
        return Triple.of(
                Math.abs(rgbValuesLeft.getLeft() - rgbValuesRight.getLeft()),
                Math.abs(rgbValuesLeft.getMiddle() - rgbValuesRight.getMiddle()),
                Math.abs(rgbValuesLeft.getRight() - rgbValuesRight.getRight())
        );
    }

    private static Triple<Integer, Integer, Integer> getRedGreenBlueValues(final int rgbValue) {
        int red = (rgbValue >> 16) & 0xff;
        int green = (rgbValue >> 8) & 0xff;
        int blue = rgbValue & 0xff;
        return Triple.of(red, green, blue);
    }

    /**
     * The idea is to generate a 'temperature' color from blue to yellow, where blue colors are more similar whereas yellow colors are more different. Accumulating these pixels will generate a heatmap.
     *
     * @param pixelDifference the total value of pixel difference should be in the range of 0 - 767 (i.e. (256 * 3) - 1, the depth of each color channel starting to count from 0).
     * @return an rgb value for a pixel that can be used in a heatmap.
     */
    private static int convertDifferenceToRGB(final int pixelDifference) {
        int rgb;
        if (pixelDifference < 256) { // Shift from blue to purple
            rgb = pixelDifference; // Red
            rgb = (rgb << 16) + 255; // Blue
        } else if (pixelDifference < ((256 * 2) - 1)) { // Shift from purple to red
            rgb = 255; // Red
            rgb = (rgb << 16) + (255 - (pixelDifference % 256)); // Blue
        } else { // Shift from red to yellow
            rgb = 255; // Red
            rgb = (rgb << 8) + (pixelDifference % 256); // Green
            rgb = (rgb << 8); // Blue
        }
        return rgb;
    }
}
