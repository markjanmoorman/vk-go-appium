#Appium setup

##Prerequisites:

- Install appium via npm install or download Appium Desktop
    - CLI
        - `npm install -g appium`
    - Appium Desktop
        - [Appium Desktop](https://github.com/appium/appium-desktop/releases)
        
##Verifying installation

To verify that all of Appium's dependencies are met you can use appium-doctor. Install it with `npm install -g appium-doctor`, then run the `appium-doctor` command to verify that all of the dependencies are set up correctly.

**Make sure that the checklist is fully checked for both iOS and/or Android otherwise Appium will not be able to execute scripts.**

##Starting Appium

- When installed Appium via the CLI you can start Appium by running the following command in the terminal: `appium`

- When using the Appium Desktop client you can click the `Start Server` button inside Appium Desktop.

Your Appium server is now running on port 4723 and is ready to execute scripts.

##Running your first test

#####Important

This example test is based on the PersgroepMobileSDK. The builds used for this test can be found at:
`/vk-go-appium/src/test/java/builds`.

Before everyting else make sure to run `yarn start` in your terminal from `/Persgroep/edition/PersgroepMobileSDK/example` 
to set up the environment. See this [README](https://bitbucket.org/persgroep/edition/src/develop/) for more information about this.

#####Setting up your runner(s)

Test case execution is done via the runners, which can be found at: `/vk-go-appium/src/test/java/runners`.

To run your test locally you can use either the `AndroidRunner` or `iOSRunner` depending which platform you want to run your test against.

Within each runner there are a set of capabilities that needs to be configured in order for the test to run on your local machine. Most capabilities are pre-defined however the following capabilities will need to be reviewed by you:

- For Android
    - `deviceName` capability needs to be set. Run `adb devices` in your terminal when the emulator is opened to retrieve your device name.
    - `avd` capability needs to be set which is the name of the avd to launch i.e. `Nexus_5X_API_27_2`.
    - `app` capability needs to be set to the location where the app is located i.e. `/vk-go-appium/src/test/java/builds/articleSdk.apk`.

- For iOS
    - `deviceName` capability needs to be set. These correspond to the simulator names in Xcode i.e. `iPhone 8`.
    - `platformVersion` capability needs to be set corresponding to the iOS version on the simulator you are using i.e. `12.1`.
    - `app` capability needs to be set to the location where the app is located i.e. `/vk-go-appium/src/test/java/builds/articleSdk.ipa`.

*More information about capabilities can be found [here](http://appium.io/docs/en/writing-running-appium/caps/)*
    
Within the runner you can configure a tag. This tag will tell the runner which test to execute. The default tag is set to `@POC-Article-SDK` which corresponds to the the test case located at: `/vk-go-appium/src/test/resources/AppiumPocPersgroepMobileSdk.feature`.

#####Executing your test

You can execute your test by running either the `AndroidRunner` or `iOSRunner` (depending on which platform you want to test against). Do make sure that the `Appium Server` is running.